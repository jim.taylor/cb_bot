# CB_chatbot
# Jim Taylor
import discord
import time
import asyncio
import pandas as pd
import sqlite3
import bot_commands
import analytics
from repost_checking import upload_images

##################
# INITIALIZATION #
##################

# Pull in api keys so this can be kept in a repository
api_keys = {}
with open('/home/borlaug/bot/api_storage.txt','r') as api:
    for line in api:
        line = line.strip('\n')
        (key, val) = line.split(',')
        api_keys[key] = val


# Initialize Discord client
TOKEN = api_keys['discord']
client = discord.Client()

# Establish connection to database
con = sqlite3.connect('/home/borlaug/bot/cb_bot/bot_files/cb_database.db')


#############
# FUNCTIONS #
#############

def log_messages(author, content):
    """ log all user messages to the database """
    localtime = time.asctime(time.localtime(time.time()))
    msglog = pd.DataFrame({'user':[author],'message':[content],'date':[localtime]})
    msglog.to_sql('messages',con,if_exists='append')


@client.event
async def on_message(message):
    """ Loop that is run on every message """
    # Ignore messages posted by the bot
    if message.author == client.user:
        return

    # Initialize container list for command outputs
    msg=[]

    # Modify user submitted message to only lowercase letters
    message_lower = message.content.lower()

    # This is processed separately from the other commands due to attaching an image
    send_trend = bot_commands.graph_trends(message_lower)
    if send_trend:
        await client.send_file(message.channel,'/home/borlaug/bot/cb_bot/reddit.png')

    #####
    # Commands can be deactivated by commenting out the line that calls them
    #####
    msg.append(bot_commands.intent(client.user,message.mentions,message_lower))
    # Get an image from Imgur based on user input
    # msg.append(bot_commands.grab_image(message_lower))
    # Repost detection
    msg.append(bot_commands.detect_repost(message.content))
    # List the available commands
    msg.append(bot_commands.list_commands(message_lower))
    # Generate a message based on ML model of our chat history
    msg.append(bot_commands.machine_learn(message_lower))
    # The bot knows how to respond if someone loves it
    msg.append(bot_commands.love(message.author, message_lower))
    # Plot trends of a topic based on reddit posts
    # msg.append(bot_commands.graph_trends(message_lower))
    # Scruffy deserved this at the time
    msg.append(bot_commands.scruffy(message_lower))
    # Legacy form of grab_image()
    msg.append(bot_commands.post_image(message_lower))
    # Grab a previous entry from our chat history
    msg.append(bot_commands.chatbot(message_lower))
    # Random chance of insulting the user
    # msg.append(bot_commands.insult())
    # Roll for check
    msg.append(bot_commands.roll(message_lower))
    ## End of main commands ##
    # Testing confidence values
    msg.append(bot_commands.confident(message_lower))
    # Reddit images
    msg.append(bot_commands.redditimg(message_lower))

    # Log messages to cb_database
    log_messages(message.author.name,message.content)
    # Upload any attachments to imgur
    upload_images(message.attachments)

    # Send the appropriate messages
    for item in msg:
        if item != False:
            await client.send_message(message.channel,item)
'''
@client.event
async def on_member_update(old_data, new_data):
    """ Runs every time an element of a member is updated """
    if old_data.game != new_data.game:
        analytics.record_gametime(new_data)
'''


@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

client.run(TOKEN)
