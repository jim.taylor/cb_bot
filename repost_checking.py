import cv2
import numpy as np
from matplotlib import pyplot as plt
import urllib
import requests


api_keys = {}
with open('/home/borlaug/bot/api_storage.txt', 'r') as api:
    for line in api:
        line = line.strip('\n')
        (key, val) = line.split(',')
        api_keys[key] = val

url = 'https://api.imgur.com/3/image'
headers = {'Authorization':api_keys['bearer']}

def check_repost(image1, image2):
    img2 = cv2.imread(image2,0)
    img1 = cv2.imread(image1,0)
    '''
    url='https://i.imgur.com/uH4rxDZ.png'
    resp = urllib.request.urlopen(url)
    test = np.asarray(bytearray(resp.read()),dtype='uint8')
    test = cv2.imdecode(test, cv2.IMREAD_COLOR)
    #img1 = cv2.resize(img1,(0,0),fx=0.5,fy=0.5)
    plt.imshow(test)
    '''
    sift = cv2.ORB_create()

    kp1, des1 = sift.detectAndCompute(img1,None)
    kp2, des2 = sift.detectAndCompute(img2,None)

    FLANN_INDEX_LSH = 6
    index_params = dict(algorithm = FLANN_INDEX_LSH,
                       table_number = 6, # 12
                       key_size = 12,     # 20
                       multi_probe_level = 1)
    search_params = dict(checks=50)

    flann = cv2.FlannBasedMatcher(index_params,search_params)

    matches = flann.knnMatch(des1,des2,k=2)

    good = []
    try:
        for m,n in matches:
            if m.distance < 0.7*n.distance:
                good.append(m)
    except ValueError:
        return False

    MIN_MATCH_COUNT = 15

    if len(good) > MIN_MATCH_COUNT:
        src_pts = np.float32([ kp1[m.queryIdx].pt for m in good]).reshape(-1,1,2)
        dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good]).reshape(-1,1,2)
        M, mask = cv2.findHomography(src_pts,dst_pts, cv2.RANSAC, 5.0)
        matchesMask = mask.ravel().tolist()
        h,w = img1.shape
        pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
        dst = cv2.perspectiveTransform(pts,M)
        img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3,cv2.LINE_AA)
        return True
    else:
        matchesMask = None
        return False


#draw_params = dict(matchColor = (0,255,0), singlePointColor = None, matchesMask = matchesMask, flags=2)

#img3 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)

#plt.imshow(img3),plt.show()

def upload_images(images):
    """ Upload images to our archive album """
    if images:
        for image in images:
            data = {'image':image['url'], 'album':'fGQ431p'}
            upload = requests.post(url, headers=headers, data=data)
