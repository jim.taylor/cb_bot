""" Analytics Module """
import sqlite3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time


# Connect to the database and get the information
con = sqlite3.connect('/home/borlaug/bot/cb_bot/bot_files/cb_database.db')
data = pd.read_sql('select * from messages',con)

def analytics():
    """ General analytics about the server. WIP """
    messages = [x for x in data['message']]
    words = []
    for m in messages:
        m = m.split(' ')
        for word in m:
            words.append(word)
    unique = len(np.unique(words))



def mystats(user):
    """ User specific analytics. WIP """
    userdata = data.loc[data['user'] == user]

def record_gametime(new_data):
    """ Builds database of gameplay time """
    game = str(new_data.game)
    user = new_data.name
    localtime = time.asctime(time.localtime(time.time()))
    previous_entry = pd.read_sql('select * from games where user = "{}"'.format(user),con).tail(1)['game'].values[0]
    if game != previous_entry:
        gamelog = pd.DataFrame({'user':[user],'game':[game],'time':[localtime]})
        gamelog.to_sql('games',con,if_exists='append')
    else:
        return
