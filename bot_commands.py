""" Command functions for the bot
    CB_Discord 2019
"""
import matplotlib
matplotlib.use("Agg")
from textgenrnn import textgenrnn
import random
from random import randint
import datetime
import praw
import requests
import matplotlib.pyplot as plt
import pandas as pd
import matplotlib.dates as mdates
import sqlite3



con = sqlite3.connect('/home/borlaug/bot/cb_bot/bot_files/cb_database.db')

command_list = ['!scruffy', '!refresh', '!trend', '!reddit', '!topic',
                '!ml', '!story', '!chatbot', '!commands', '!vote', '!trend',
                '!confident', '!rimage'
               ]

# Initialize word lists
with open('/home/borlaug/bot/cb_bot/bot_files/insults.txt', 'r') as insults:
    words_resp = insults.readlines()
words_resp = [x.strip('\n') for x in words_resp]

kid = 'Kid still not here.'

api_keys = {}
with open('/home/borlaug/bot/api_storage.txt', 'r') as api:
    for line in api:
        line = line.strip('\n')
        (key, val) = line.split(',')
        api_keys[key] = val

def initialize_services():
    """ Initialize all services and variables that the commands will use """
    # Initialize imgur api
    url = 'https://api.imgur.com/3/gallery/search/'
    headers = {'Authorization':api_keys['imgur']}

    # Initialize machine learning text generation
    textgen = textgenrnn(
        weights_path='/home/borlaug/bot/cb_bot/cbchat_full_nourl_weights.hdf5',
        vocab_path='/home/borlaug/bot/cb_bot/cbchat_full_nourl_vocab.json',
        config_path='/home/borlaug/bot/cb_bot/cbchat_full_nourl_config.json'
        )

    # Generate response cache
    responses = textgen.generate(50, return_as_list=True, temperature=1)
    cached_responses = []
    for i in responses:
        if len(i) > 3:
            cached_responses.append(i)

    # Reddit bot to to generate comic links from user Sau-Seige
    reddit = praw.Reddit(client_id='ibQPTVmj_m9EZQ', client_secret=api_keys['reddit'],
                         user_agent='CB_Siegebot_for_annoying_chris')
    sausiege = reddit.redditor('sau-siege')
    karma = sausiege.link_karma
    comics = sausiege.submissions.new()
    comiclinks = {}
    for x in comics:
        if 'i.redd.it' in x.url:
            comiclinks[x.url] = x.score

    # Get past channel chats
    with open('/home/borlaug/bot/cb_bot/bot_files/cb_chat_nourl.txt', 'r') as chatlog:
        chats = chatlog.read().splitlines()

    return url, headers, textgen, cached_responses, comiclinks, chats, reddit, karma

def refresh():
    """ Refresh the cache of machine learned text responses """
    r = textgen.generate(50, return_as_list=True, temperature=1)
    [cached_responses.append(x) for x in r]

def list_commands(content):
    """ Command to display all the available bot commands """
    if '!commands' in content:
        msg = ("!spooky - Posts a spooky image \n"
               "!refresh - Generates fresh Machine Learning Responses \n"
               "!trend - google trend comparison of topics (use commas to seperate) \n"
               "!reddit - reddit trend comparison of topics (use commas to seperate) \n"
               "!topic - imgur post with your defined topic tags \n"
               "!ml - generates a machine learned message from this channel \n"
               "!chatbot - post a previous message from this channel \n"
               "\n Note: \n This bot may insult you randomly, "
               "notify if your weblink has been previously posted or unconditionally love you."
              )
    else:
        msg = False
    return msg

def detect_repost(content):
    """ Checks if a link has been sent previously """
    if "http" in content or 'https' in content:
        df = pd.read_sql('select * from messages', con)
        messages = [x for x in df['message']]
        if content in messages:
            idx = max(df.index[df['message'] == content].tolist())
            repost = df.loc[idx]
            msg = "REEEEEEEEEPOST Last posted {} by {}".format(repost['date'], repost['user'])
        else:
            msg = False
    else:
        msg = False
    return msg
'''
def grab_image(content):
    """ Grabs image from imgur related to the topic the user inputs """
    if content.startswith('!') and content.split(' ')[0] not in command_list:
        topic = content.split('!')[1]
        topic = topic.split(' ')
        querystring = {'q':'+'.join(topic),'nsfw':'true'}
        response = requests.request('GET', url, headers=headers, params=querystring)
        json_data = response.json()
        results = [json_data['data'][x] for x in range(len(json_data['data']))]
        links = []
        for album in results:
            try:
                for image in album['images']:
                    links.append(image['link'])
            except KeyError:
                    links.append(album['link'])
        try:
            msg = links[randint(0, len(links)-1)]
        except ValueError:
            msg = "No images for that query."
        return msg
    else:
        return False
'''
def machine_learn(content):
    """ Command to generate and post a machine learned message based on past user messages """
    if '!ml' in content:
        # Keep cache populated with more than 5 responses
        if len(cached_responses) < 5:
            refresh()

        # Randomly select a cached response and make sure it's not blank
        idx = randint(0, len(cached_responses)-1)
        if cached_responses[idx] == "" or cached_responses[idx] == " ":
            cached_responses.remove(cached_responses[idx])
            idx = randint(0, len(cached_responses)-1)
        msg = cached_responses[idx]
    else:
        msg = False
    return msg

def love(author, content):
    """ Return a message of love to the user."""
    if 'love' in content and any(x in ['bot', 'robit', 'robot'] for x in content.split()):
        user = str(author).split('#')
        msg = ' '.join(["I love", user[0]])
    else:
        msg = False
    return msg

def graph_trends(content):
    """ Command to genereate trend graph based on Reddit data (Use a comma to compare multiple trends) """
    if content.startswith('!reddit') or content.startswith('!trend'):
        message_split = content.split()
        topics = ' '.join(message_split[1:len(message_split)])
        query = topics.split(',')
        time = 365*4
        matplotlib.style.use('seaborn-poster')
        fig, ax = plt.subplots()
        for q in query:
            testreq = requests.get('https://api.pushshift.io/reddit/search/submission/?q={}&after={}d&aggs=created_utc&frequency=week&size=0'.format(q, time))
            r = testreq.json()
            rdf = pd.DataFrame.from_dict(r['aggs']['created_utc'])
            rdf['doc_count'] = rdf['doc_count']/rdf.mean()[0]
            years = mdates.YearLocator()
            months = mdates.MonthLocator()
            dates = [pd.to_datetime(datetime.datetime.utcfromtimestamp(rdf['key'][x]).strftime('%Y-%m-%d')) for x, z in enumerate(rdf['key'])]
            rdf = rdf.rename(columns={"doc_count":q})
            ax.plot(dates, rdf[q])
            ax.xaxis.set_major_locator(years)
            ax.xaxis.set_minor_locator(months)
        ax.set_title('Reddit interest over time')
        ax.set_ylabel('Normalized post count')
        ax.set_xlabel('Date')
        plt.legend()
        plt.savefig('/home/borlaug/bot/cb_bot/reddit.png')
        plt.gcf().clear()
        plt.close()
        msg = True
    else:
        msg = False
    return msg



def scruffy(content):
    """ Command to post a Sau_Seige comic that annoys Scruffy"""
    if '!scruffy' in content:
        link, post_karma = random.choice(list(comiclinks.items()))
        msg = ('This post received {} points\n {}'.format(post_karma, link))

    else:
        msg = False
    return msg

def post_image(content):
    """ Command to post an image from imgur with a defined topic tag """
    if content.startswith('!topic'):
        message_split = content.split()
        topic = message_split[1:len(message_split)]
        querystring = {'q':'+'.join(topic),'nsfw':'true'}
        response = requests.request('GET', url, headers=headers, params=querystring)
        json_data = response.json()
        topiclinks = [json_data['data'][x]['link'] for x in range(len(json_data['data']))]
        try:
            msg = topiclinks[randint(0, len(topiclinks)-1)]
        except ValueError:
            msg = "No images for that query."
    else:
        msg = False
    return msg

def chatbot(content):
    """ Command that sends a previously submitted user message """
    if content.startswith('!chatbot'):
        msg = chats[randint(0, len(chats))]
    else:
        msg = False
    return msg

def intent(user, mentions, content):
    """ Check to see if the bot was mentioned and insulted then respond negatively. """
    if user in mentions:
        hostile = False
        params = {'text': content}
        r = requests.post('http://text-processing.com/api/sentiment/', data=params)
        data = r.json()
        if data['label'] == 'neg':
            hostile = True
        if hostile:
            msg = words_resp[randint(0, len(words_resp)-1)]
        else:
            msg = textgen.generate(1, return_as_list=True, temperature=1.75)[0]
    else:
        msg = False
    return msg

def insult():
    """ Robit randomly responds with an insult to the previous message """
    roll = randint(0, 200)
    with open('/home/borlaug/bot/cb_bot/bot_files/bot_banter.txt', 'r') as banter:
        insults = banter.read().splitlines()
    if roll == randint(0, 200):
        msg = insults[randint(0, len(insults))]
    else:
        msg = False
    return msg

def roll(content):
    if 'roll for' in content:
        check = content.split('roll for ')[1]
        randroll = randint(1, 20)
        msg = 'You rolled {} for {}'.format(randroll, check)
    else:
        msg = False
    return msg

def redditimg(content):
    """ Use reddit as image source instead of imgur """
    #if content.startswith('!rimage') and content.split(' ')[0] not in command_list:
    if content.startswith('!') and content.split(' ')[0] not in command_list:
        topic = content.split('!')[1]
        #topic = content.split('!rimage')[1]
        rall = reddit.subreddit('all')
        imagelist = []
        for image in rall.search(topic, limit=100, params={'nsfw':'1', 'include_over_18':'on'}):
            url = image.url
            if url.split('.')[-1] in ['jpg','gif','gifv','mp4','png']:
                if image.over_18:
                    imagelist.append('||{}||'.format(image.url))
                else:
                    imagelist.append(image.url)
        try:
            msg = imagelist[randint(0, len(imagelist)-1)]
        except ValueError:
            msg = "No images for that query."
    else:
        msg = False
    return msg

    ################
    # EXPERIMENTAL #
    ################

def vote(content):
    """ Still in progress. """
    if content.startswith('!vote'):
        voteinfo = content.split(',')
        options = voteinfo[1:]
        title = ' '.join(voteinfo[0].split()[1:])
        msg = [title]
        [msg.append(x) for x in options]

def confident(content):
    """ Demonstration of a message the bot is confident we would say """
    if content.startswith('!confident'):
        msg = textgen.generate(1, return_as_list=True, temperature=0.2)[0]
    else:
        msg = False
    return msg




url, headers, textgen, cached_responses, comiclinks, chats, reddit, karma = initialize_services()
