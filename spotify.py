""" Spotify module """
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials

client_credentials_manager = SpotifyClientCredentials(
    client_id='ba66f984960a4ec5b1bb4e54e4bfb4f6',
    client_secret='dd3d25fefaff4df5886e72f0d91ee305'
    )
sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

results = sp.search(q='artist:' + 'george strait', type='artist')
uri = results['artists']['items'][0]['uri']

tracks = sp.artist_top_tracks(uri)
artist = tracks['tracks'][0]['artists'][0]['name']

print('Top 10 Tracks by {}'.format(artist))
for track in tracks['tracks'][:1]:
    url = track['uri'].replace(':', '/')
    url = url.replace('spotify', 'https://open.spotify.com')
    print(track['name'], url)
